#!/usr/bin/python

from __future__ import print_function
import sys
from Task 	import Task
from Graph 	import Graph
from TaskChain 	import TaskChain
from Resource 	import Resource
# ---- Declaring global variable
taskList = []
adjacencyList = {}
graph = Graph(adjacencyList)
criticalPaths = []
taskChains = []
resources = []
globalFreeSlots = []

# ---- Read data from files 
def importTasks():

# ---  Read tasks from files.	
	global taskList
	global resources
	fptasks = open("tasks.txt","r")

	for line in fptasks.readlines():
		newTask = Task()
		t = line.split(",")	
		newTask.id = int(t[0])
		newTask.name = t[1]
		newTask.duration = int(t[2])
		adjacencyList[newTask.id] = []
		taskList.append(newTask)	
	fptasks.close()			

#---- Read resources
	fpresources = open("resources.txt","r")
	
	for line in fpresources.readlines():
		r = line.strip().split(" ")
		newResource = Resource(int(r[0]),r[1])
		resources += [newResource]
				
	fpresources.close()

# ---- Read depedencies and create predecessor list -----
	fpdependencies = open("dependencies.txt","r")	

	for line in fpdependencies.readlines():
		x = line.split(" ")
		
		if len(x) >1:			
			z = x[1]		
			for task in taskList:
				if task.id == int(x[0]):			 					
					y = z.strip().split(",")	
					for b in y:
						task.predecessors.append(int(b))								
				#print(x[0],task.predecessor)
	fpdependencies.close()


# ---- Create successor list ----
	for succTask in taskList:
		#succTask = getTaskFromId()
		for predId in succTask.predecessors:
			task = getTaskFromId(predId)
			task.successors.append(succTask.id)
 			adjacencyList[task.id] = task.successors
 			

# -- Print id, predecessor, successor	
"""for task in taskList:	
	print(task.id,task.predecessors,task.successors)"""
	
# -- Main Functionality implementation

def getTaskFromId(id):
	for task in taskList:
		if task.id == id:
			return task 
	return None


# -- find all critical paths 
def findCriticalPaths():
	
	noPredTasks = []
	noSuccTasks = []
	noPredSuccTasks = []
	global criticalPaths
	for task in taskList:
		if len(task.predecessors) == 0 and len(task.successors) == 0:
			
			noPredSuccTasks.append(task.id)
			
		# -- find no predecessor tasks
		elif len(task.predecessors) == 0:
			noPredTasks.append(task.id)
		
		# -- find no successor tasks	 
		elif len(task.successors) == 0:
			noSuccTasks.append(task.id)
	
	
	print (noPredTasks)
	print (noSuccTasks)
	# -- find a path between each pair
	for id1 in noPredTasks:
		for id2 in noSuccTasks:
			criticalPaths += graph.find_all_paths(id1,id2)	
	
 	# -- add remaining tasks
 	
 	for id1 in noPredSuccTasks:
 		singleNodeList = [id1]
 		criticalPaths += [singleNodeList]
 	
 	print  (criticalPaths)	 

def getTotalPathDuration(path):
	
	duration = 0
	for id in path:
		task = getTaskFromId(id)
		duration +=task.duration
	return duration

def findAndSortTaskChains():
	
	global taskChains
	
	for path in criticalPaths:
		#print (path)
		taskChain = TaskChain(path)
		taskChain.totalDuration = getTotalPathDuration(path)
		taskChains += [taskChain]
	
	
	# -- Sort the chains in descending order.
	taskChains.sort(key = lambda x:x.totalDuration, reverse = True)
	
	
	# -- Initialize the start days for all tasks.
	for taskChain in taskChains:
		taskChain.tasks[0].startDay = 1
		taskChain.tasks[0].endDay = taskChain.tasks[0].startDay + taskChain.tasks[0].duration - 1
			for task in taskChain.tasks:
				if  taskChain.tasks.index(task) > 1: 
	
	for taskChain in taskChains:
		print(taskChain.tasks)
		  
# -- Only thing remaining 	
def allocateResources():
	
	global globalFreeSlots
	for resource in resources:
		for freeSlot in resource.freeSlots:
			globalFreeSlots += [freeSlot]
	
	# -- sort the global free slots
	globalFreeSlots.sort(key = lambda x:(x.start,x.slotDuration),reverse = False)
	
	for taskChain in taskChains:
		for task in taskChain:
			if task.resourceOnTask == None:
				getSuitableSlotDuration(task,false)
				
				if task.resourceOnTask == None:
					getSuitableSlotDuration(task,true)
					

def adjustSuccessors(task):
	
	for succId in task.successors:
		succTask = getTaskFromId(succId)
		if succTask.startDay <= task.endDay
			succTask.startDay = task.endDay + 1
			succTask.endDay = succTask.startDay + succTask.duration -1 	
						
					
def getSuitableSlotDuration(task,flag):
	for freeSlot in globalFreeSlots:
		if freeSlot.slotDuration and task.startDay >= freeSlot.start and task.endDay<= freeSlot.end:
			task.resourceOnTask = freeSlot.resourceOnSlot
			if flag == true:
				task.startDay = freeSlot.start
				task.endDay = task.startDay + freeSlot.slotDuration-1
				adjustSuccessors(task) 
			if task.startDay - freeSlot.start>0:	
				remFreeSlot1 = Slot(freeSlot.start, task.startDay-1)
				remFreeSlot1.resourceOnSlot = freeSlot.resourceOnSlot
				globalFreeSlots.append(remFreeSlot1)
				#remFreeSlot1.resourceOnSlot.freeSlots.remove(freeSlot)
				remFreeSlot1.resourceOnSlot.freeSlots.append(remFreeSlot1)
						
			if freeSlot.end - task.endDay>0:	
				remFreeSlot2 = Slot(task.endDay, freeSlot.end -1)
				remFreeSlot2.resourceOnSlot = freeSlot.resourceOnSlot
				globalFreeSlots.append(remFreeSlot2)
				#remFreeSlot2.resourceOnSlot.freeSlots.remove(freeSlot)
				remFreeSlot2.resourceOnSlot.freeSlots.append(remFreeSlot2)
						
			freeSlot.resourceOnSlot.freeSlots.remove(freeSlot)
			globalFreeSlots.remove(freeSlot) 
			globalFreeSlots.sort(key = lambda x:(x.start,x.slotDuration),reverse = False)
			
			return
			
	return
					
	""" for freeSlot in globalFreeSlots:
		print(freeSlot.resourceOnSlot.name," ",freeSlot.start," ",freeSlot.end) """ 

def main():
	importTasks() 
	findCriticalPaths()
	findAndSortTaskChains()		
	allocateResources()
	
main()

